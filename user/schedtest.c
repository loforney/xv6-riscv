// Test that fork fails gracefully.
// Tiny executable so that the limit can be filling the proc table.

#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/pstat.h"

#define N 2

void
print(const char *s)
{
  write(1, s, strlen(s));
}

int pid1 = -1;
int pid2 = -1;

int
main(void)
{

  int rc = fork();

  if (rc < 0) {
      print("Fork failed\n");
  } else if (rc == 0) {
      print("Hi I'm the child1\n");
      nice(14); // Set nice value
      pid1 = (int) getpid(); // Get PID
      fprintf(2, "My PID is: %d\n", pid1);
      while (1) {} // Create infinite loop
  }

  int rc2 = fork();

  if (rc2 < 0) {
      print("Fork failed\n");
  } else if (rc2 == 0) {
      print("Hi I'm the child2\n");
      nice(19); // Set nice value
      pid2 = (int) getpid(); // Get PID
      fprintf(2, "My PID is: %d\n", pid2);
      while (1) {} // Create infinite loop
  }

  if (rc > 0 && rc2 > 0) {
      struct pstat kpstat;

      while (1) {
          sleep(10); // Sleep and let the children do the work, like were in the 1950's
          getpstat(&kpstat); // Update kpstat element

          for (int i = 0; i < NPROC; i++) {
              if (kpstat.nice[i] == 14) { // If the nice value is 14, then it's the first child
                  fprintf(2, "Child 1 Run: %d, Pass: %d, Stride: %d ", kpstat.runtime[i], kpstat.pass[i], kpstat.stride[i]);
              } else if (kpstat.nice[i] == 19) { // If nice value is 19, then it's the second child
                  fprintf(2, "Child 2 Run: %d, Pass: %d, Stride: %d ", kpstat.runtime[i], kpstat.pass[i], kpstat.stride[i]);
              } // Print all variables of process
          }
          print("\n");
      }
  }

  exit(0);
}
