#include "kernel/param.h"
#include "kernel/pstat.h"
#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

//
// Created by leo on 2/11/22.
//

int
main(int argc, char *argv[]) {
    struct pstat kpstat;
    getpstat(&kpstat);
    for (int i = 0; i < NPROC; i++) {
        fprintf(2, "[%d]: INUSE: %d, NICE: %d, PID: %d, RT: %d, PASS: %d, STR: %d\n", i, kpstat.inuse[i], kpstat.nice[i], kpstat.pid[i], kpstat.runtime[i], kpstat.pass[i], kpstat.stride[i]);
    }
    exit(0);
}
