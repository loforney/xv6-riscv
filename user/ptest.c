// Create a zombie process that
// must be reparented at exit.
// Leo Forney

#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int
main(void) {
    fprintf(2, "%d\n", pcount()); // Expected 3. Val 3. One for kernel, one for shell, and one for the program.
    if (fork() > 0) {
        sleep(5);// Let child exit before parent.
        fprintf(2, "%d\n", pcount()); // Expected 4. Val 4. Kernel, sh, main program, and fork. They should be different.
    }
    exit(0);
}
