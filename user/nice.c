// Leo Forney

#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int
main(int argc, char *argv[]) {
    if (argc > 2) {
        int niceValParsed = atoi(argv[1]);
        nice(niceValParsed);
        exec(argv[2], &argv[2]);
    }
    exit(0);
}
